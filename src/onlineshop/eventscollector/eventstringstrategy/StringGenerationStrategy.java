package onlineshop.eventscollector.eventstringstrategy;

import java.util.Map;
import onlineshop.eventscollector.events.Event;

/**
 *
 * @author Hiten Vasnani
 */
public interface StringGenerationStrategy {

    public String getString(Event event, Map<String, String> eventAttributes);
}
