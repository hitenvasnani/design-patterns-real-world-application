package onlineshop.eventscollector.events;

import onlineshop.eventscollector.eventvisitor.EventVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public class UserDeactivatedEvent extends Event {

    public static final String EVENT_TYPE_TAG = "USER_DEACTIVATED";

    private final String country;

    public UserDeactivatedEvent(long occurenceTime, String country) {
        super(occurenceTime, EVENT_TYPE_TAG);
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public <T> T accept(EventVisitor<T> eventVisitor) {
        return eventVisitor.visit(this);
    }
}
