package onlineshop.eventscollector.eventvisitor.statistics;

import onlineshop.eventscollector.events.AddedToCartEvent;
import onlineshop.eventscollector.events.OutOfStockEvent;
import onlineshop.eventscollector.events.PurchasedEvent;
import onlineshop.eventscollector.events.ReturnedEvent;
import onlineshop.eventscollector.events.UserDeactivatedEvent;
import onlineshop.eventscollector.events.UserRegisteredEvent;

/**
 *
 * @author Hiten Vasnani
 */
public class EventCountStatisticsVisitor extends StatisticsEventVisitor {

    int userRegisteredEventCount;
    int userDeactivatedEventCount;
    int outOfStockEventCount;
    int addedToCartEventCount;
    int purchasedEventCount;
    int returnedEventCount;

    public EventCountStatisticsVisitor() {
        userRegisteredEventCount = 0;
        userDeactivatedEventCount = 0;
        outOfStockEventCount = 0;
        addedToCartEventCount = 0;
        purchasedEventCount = 0;
        returnedEventCount = 0;
    }

    @Override
    public Void visit(UserRegisteredEvent userRegisteredEvent) {
        userRegisteredEventCount++;
        return null;
    }

    @Override
    public Void visit(UserDeactivatedEvent userDeactivatedEvent) {
        userDeactivatedEventCount++;
        return null;
    }

    @Override
    public Void visit(OutOfStockEvent outOfStockEvent) {
        outOfStockEventCount++;
        return null;
    }

    @Override
    public Void visit(AddedToCartEvent addedToCartEvent) {
        addedToCartEventCount++;
        return null;
    }

    @Override
    public Void visit(PurchasedEvent purchasedEvent) {
        purchasedEventCount++;
        return null;
    }

    @Override
    public Void visit(ReturnedEvent returnedEvent) {
        returnedEventCount++;
        return null;
    }

    @Override
    public String getReport() {
        String report = "";
        final String lineFormat = "%-20s%d\n";
        report += String.format(lineFormat, UserRegisteredEvent.EVENT_TYPE_TAG, userRegisteredEventCount);
        report += String.format(lineFormat, UserDeactivatedEvent.EVENT_TYPE_TAG, userDeactivatedEventCount);
        report += String.format(lineFormat, OutOfStockEvent.EVENT_TYPE_TAG, addedToCartEventCount);
        report += String.format(lineFormat, AddedToCartEvent.EVENT_TYPE_TAG, outOfStockEventCount);
        report += String.format(lineFormat, PurchasedEvent.EVENT_TYPE_TAG, purchasedEventCount);
        report += String.format(lineFormat, ReturnedEvent.EVENT_TYPE_TAG, returnedEventCount);
        return report;
    }

}
