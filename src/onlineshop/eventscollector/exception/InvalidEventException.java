package onlineshop.eventscollector.exception;

/**
 *
 * @author Hiten Vasnani
 */
public class InvalidEventException extends Exception {

    public InvalidEventException(String message) {
        super(message);
    }
}
