package onlineshop.eventscollector.eventnotification;

import onlineshop.eventscollector.events.Event;
import onlineshop.eventscollector.eventstringstrategy.ConsoleLogStringGenerationStrategy;
import onlineshop.eventscollector.eventvisitor.StringGenerationVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public class ConsoleLoggingEventObserver implements EventObserver {

    private final StringGenerationVisitor consoleLoggingVisitor;

    public ConsoleLoggingEventObserver() {
        this.consoleLoggingVisitor = new StringGenerationVisitor(new ConsoleLogStringGenerationStrategy());
    }

    @Override
    public void notifyEventOccured(Event event) {
        String consolelog = event.accept(consoleLoggingVisitor);
        System.out.println(consolelog);
    }
}
