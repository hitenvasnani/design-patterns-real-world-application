package onlineshop.eventscollector.eventnotification;

import java.util.Collection;
import java.util.HashSet;
import onlineshop.eventscollector.events.Event;
import onlineshop.eventscollector.eventvisitor.statistics.StatisticsEventVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public class StatisticsGenerationEventObserver implements EventObserver {

    private final Collection<StatisticsEventVisitor> statisticsList;

    public StatisticsGenerationEventObserver() {
        this.statisticsList = new HashSet<StatisticsEventVisitor>();
    }

    public void addStatisticsGenerator(StatisticsEventVisitor statisticsVisitor) {
        this.statisticsList.add(statisticsVisitor);
    }

    public void removeStatisticsGenerator(StatisticsEventVisitor statisticsVisitor) {
        this.statisticsList.remove(statisticsVisitor);
    }

    @Override
    public void notifyEventOccured(Event event) {
        for (StatisticsEventVisitor statisticsVisitor : statisticsList) {
            event.accept(statisticsVisitor);
        }
    }

}
