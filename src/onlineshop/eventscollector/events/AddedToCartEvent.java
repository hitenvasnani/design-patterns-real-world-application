package onlineshop.eventscollector.events;

import onlineshop.eventscollector.eventvisitor.EventVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public class AddedToCartEvent extends Event {

    public static final String EVENT_TYPE_TAG = "ADDED_TO_CART";

    private final long productId;
    private final int quantity;

    public AddedToCartEvent(long occurenceTime, long productId, int quantity) {
        super(occurenceTime, EVENT_TYPE_TAG);
        this.productId = productId;
        this.quantity = quantity;
    }

    public long getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public <T> T accept(EventVisitor<T> eventVisitor) {
        return eventVisitor.visit(this);
    }
}
