package onlineshop.logger;
import onlineshop.eventscollector.EventsCollector;
import onlineshop.eventscollector.exception.InvalidEventException;
/**
 *
 * @author Hiten Vasnani
 */
public class LogFileHelper {
    
    public static void log(String logData){
        try{
            EventsCollector.collectEvent(logData);
        }catch(InvalidEventException e){
            System.out.println(logData);
        }
    }
}
