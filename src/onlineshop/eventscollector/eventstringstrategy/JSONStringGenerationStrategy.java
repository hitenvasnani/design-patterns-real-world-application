package onlineshop.eventscollector.eventstringstrategy;

import java.util.Map;
import onlineshop.eventscollector.events.Event;

/**
 *
 * @author Hiten Vasnani
 */
public class JSONStringGenerationStrategy implements StringGenerationStrategy {

    @Override
    public String getString(Event event, Map<String, String> eventAttributes) {
        String jsonString = "{\n";
        jsonString += "\"event_type\": \"" + event.getType() + "\",\n";
        jsonString += "\"occurence_time\": \"" + event.getOccurenceTime() + "\"";
        for (Map.Entry<String, String> entry : eventAttributes.entrySet()) {
            jsonString += ",\n\"" + entry.getKey() + "\": \"" + entry.getValue() + "\"";
        }
        jsonString += "\n}";
        return jsonString;
    }
}
