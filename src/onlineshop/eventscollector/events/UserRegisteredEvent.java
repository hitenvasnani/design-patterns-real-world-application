package onlineshop.eventscollector.events;

import onlineshop.eventscollector.eventvisitor.EventVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public class UserRegisteredEvent extends Event {

    public static final String EVENT_TYPE_TAG = "USER_REGISTERED";

    private final String country;

    public UserRegisteredEvent(long occurenceTime, String country) {
        super(occurenceTime, EVENT_TYPE_TAG);
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public <T> T accept(EventVisitor<T> eventVisitor) {
        return eventVisitor.visit(this);
    }

}
