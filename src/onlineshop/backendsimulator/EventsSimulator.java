package onlineshop.backendsimulator;

/**
 *
 * @author Hiten Vasnani
 */
public class EventsSimulator {

    public static void main(String[] args) {
        int numberOfThreadsToStart = RandomValuesHelper.getRandomNumber(3, 10);
        while (numberOfThreadsToStart-- > 0) {
            Thread eventsGenerationThread = new Thread(new RandomEventsGenerator());
            eventsGenerationThread.start();
        }
    }

}
