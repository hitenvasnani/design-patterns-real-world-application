package onlineshop.eventscollector.eventnotification;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import onlineshop.eventscollector.events.Event;
import onlineshop.eventscollector.eventstringstrategy.JSONStringGenerationStrategy;
import onlineshop.eventscollector.eventvisitor.StringGenerationVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public class JSONFileEventObserver implements EventObserver {

    private final StringGenerationVisitor jsonStringVisitor;
    private final String jsonFilePath;

    public JSONFileEventObserver(String jsonFilePath) {
        this.jsonStringVisitor = new StringGenerationVisitor(new JSONStringGenerationStrategy());
        this.jsonFilePath = jsonFilePath;
    }

    @Override
    public void notifyEventOccured(Event event) {
        String jsonString = event.accept(jsonStringVisitor);
        appendToFile(jsonString);
    }

    private synchronized void appendToFile(String jsonString) {
        File file = new File(jsonFilePath);
        boolean fileExists = file.exists();
        file.getParentFile().mkdirs();
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new FileWriter(file, true));
            output.append((fileExists ? (", \n") : "") + jsonString);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
