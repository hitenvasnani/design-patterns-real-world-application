package onlineshop.eventscollector.eventvisitor.statistics;

import onlineshop.eventscollector.eventvisitor.EventVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public abstract class StatisticsEventVisitor implements EventVisitor<Void>{
    
    public abstract String getReport();
}
