package onlineshop.eventscollector.eventnotification;

import java.util.Collection;
import java.util.HashSet;
import onlineshop.eventscollector.events.Event;

/**
 *
 * @author Hiten Vasnani
 */
public class EventPublisher {

    private final Collection<EventObserver> eventObservers;

    public EventPublisher() {
        this.eventObservers = new HashSet<EventObserver>();
    }

    public void registerEventObserver(EventObserver eventObserver) {
        this.eventObservers.add(eventObserver);
    }

    public void unRegisterEventObserver(EventObserver eventObserver) {
        this.eventObservers.remove(eventObserver);
    }

    public void publishEvent(Event event) {
        for (EventObserver eventObserver : eventObservers) {
            eventObserver.notifyEventOccured(event);
        }
    }

}
