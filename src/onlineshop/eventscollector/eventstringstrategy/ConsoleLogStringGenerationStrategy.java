package onlineshop.eventscollector.eventstringstrategy;

import java.util.Map;
import onlineshop.eventscollector.events.Event;

/**
 *
 * @author Hiten Vasnani
 */
public class ConsoleLogStringGenerationStrategy implements StringGenerationStrategy {

    private final static String EVENT_TYPE_SEPARATOR = ":";
    private final static String EVENT_ATTRIBUTES_DELIMITER = ", ";

    @Override
    public String getString(Event event, Map<String, String> eventAttributes) {
        String consoleLog = event.getType();
        consoleLog += EVENT_TYPE_SEPARATOR;
        consoleLog += event.getOccurenceTime() + "";
        for (String attribute : eventAttributes.values()) {
            consoleLog += EVENT_ATTRIBUTES_DELIMITER;
            consoleLog += attribute;
        }
        return consoleLog;
    }
}
