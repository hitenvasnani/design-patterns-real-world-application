package onlineshop.backendsimulator;

import onlineshop.logger.LogFileHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Hiten Vasnani
 */
public class RandomEventsGenerator implements Runnable {

    private static final String[] VALID_EVENT_TYPES = {"USER_REGISTERED", "USER_DEACTIVATED", "OUT_OF_STOCK", "ADDED_TO_CART", "PURCHASED", "RETURNED"};
    private static final String[] COUNTRIES = {"USA", "UK", "AUSTRALIA", "NEW ZEALAND", "JAPAN", "CHINA", "INDIA", "KOREA", "INDONESIA", "PHILIPPINES"};

    private static final String[] INVALID_EVENT_TYPES = {"DEBUG", "TRIAL", "TEST"};
    private static final String[] INVALID_DATA = {"object is not null", "success", "method exited", "method entered"};
    private static final String[] INVALID_DELIMITERS = {":", "-", "---->", " "};

    @Override
    public void run() {
        while (true) {
            try {
                generateAndSendRandomEvent();
                Thread.sleep(RandomValuesHelper.getRandomNumber(1000, 4000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void generateAndSendRandomEvent() {
        boolean toGeneateValidEvent = RandomValuesHelper.getRandomBoolean(0.9);
        String eventData = "";
        if (toGeneateValidEvent) {
            eventData = generateValidEvent();
        } else {
            eventData = generateInvalidEvent();
        }
        LogFileHelper.log(eventData);
    }

    private String generateValidEvent() {
        String eventTag = RandomValuesHelper.getRandomArrayValue(VALID_EVENT_TYPES);
        List<String> values = new ArrayList<String>();
        if (eventTag.equalsIgnoreCase(VALID_EVENT_TYPES[0]) || eventTag.equalsIgnoreCase(VALID_EVENT_TYPES[1])) { //user registered, user deactivated
            values.add(RandomValuesHelper.getRandomArrayValue(COUNTRIES)); //country
        } else if (eventTag.equalsIgnoreCase(VALID_EVENT_TYPES[2]) || eventTag.equalsIgnoreCase(VALID_EVENT_TYPES[3])) { //out of stock, added to cart
            values.add("" + RandomValuesHelper.getRandomNumber(1, 1000)); //id
            values.add("" + RandomValuesHelper.getRandomNumber(1, 10)); //qty
        } else if (eventTag.equalsIgnoreCase(VALID_EVENT_TYPES[4]) || eventTag.equalsIgnoreCase(VALID_EVENT_TYPES[5])) { //purchased, returned
            values.add("" + RandomValuesHelper.getRandomNumber(1, 1000)); //id
            values.add("" + RandomValuesHelper.getRandomNumber(1, 10)); //qty
            values.add(RandomValuesHelper.getRandomArrayValue(COUNTRIES)); //country
        }
        return eventTag + ":" + join(values.iterator());
    }

    private String generateInvalidEvent() {
        String eventTag = RandomValuesHelper.getRandomArrayValue(INVALID_EVENT_TYPES);
        List<String> values = new ArrayList<String>();
        values.add(RandomValuesHelper.getRandomArrayValue(INVALID_DATA));
        int numbersToAdd = RandomValuesHelper.getRandomNumber(1, 5);
        while (numbersToAdd-- > 0) {
            values.add("" + RandomValuesHelper.getRandomNumber(0, 100));
        }
        String delim = RandomValuesHelper.getRandomArrayValue(INVALID_DELIMITERS);
        return eventTag + delim + join(values.iterator());
    }

    private static String join(Iterator<String> valuesIterator) {
        final String delimeter = ", ";
        String joinedString = "";
        while (valuesIterator.hasNext()) {
            String value = valuesIterator.next();
            joinedString += value;
            if (valuesIterator.hasNext()) {
                joinedString += delimeter;
            }
        }
        return joinedString;
    }
}
