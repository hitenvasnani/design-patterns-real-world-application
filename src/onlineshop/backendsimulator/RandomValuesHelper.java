package onlineshop.backendsimulator;

/**
 *
 * @author Hiten Vasnani
 */
public class RandomValuesHelper {

    public static final int getRandomNumber(int min, int max) {
        return min + (int) (Math.random() * (max - min));
    }

    public static final <T> T getRandomArrayValue(T[] array) {
        int randomIndex = getRandomNumber(0, array.length);
        return array[randomIndex];
    }

    public static boolean getRandomBoolean(double trueProbability) {
        final int precisionScale = 10000;
        int randomNumber = getRandomNumber(0, precisionScale);
        double threshHold = trueProbability * precisionScale;
        return randomNumber <= threshHold;
    }

}
