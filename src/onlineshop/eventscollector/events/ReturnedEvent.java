package onlineshop.eventscollector.events;

import onlineshop.eventscollector.eventvisitor.EventVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public class ReturnedEvent extends Event {

    public static final String EVENT_TYPE_TAG = "RETURNED";

    private final long productId;
    private final int quantity;
    private final String country;

    public ReturnedEvent(long occurenceTime, long productId, int quantity, String country) {
        super(occurenceTime, EVENT_TYPE_TAG);
        this.productId = productId;
        this.quantity = quantity;
        this.country = country;
    }

    public long getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public <T> T accept(EventVisitor<T> eventVisitor) {
        return eventVisitor.visit(this);
    }
}
