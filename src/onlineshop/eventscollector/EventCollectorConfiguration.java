package onlineshop.eventscollector;

import java.util.Timer;
import onlineshop.eventscollector.eventnotification.ConsoleLoggingEventObserver;
import onlineshop.eventscollector.eventnotification.EventObserver;
import onlineshop.eventscollector.eventnotification.EventPublisher;
import onlineshop.eventscollector.eventnotification.JSONFileEventObserver;
import onlineshop.eventscollector.eventnotification.StatisticsGenerationEventObserver;
import onlineshop.eventscollector.eventvisitor.statistics.EventCountStatisticsVisitor;
import onlineshop.eventscollector.eventvisitor.statistics.StatisticsEventVisitor;
import onlineshop.eventscollector.eventvisitor.statistics.StatisticsReportTimerTask;

/**
 *
 * @author Hiten Vasnani
 */
public class EventCollectorConfiguration {

    static EventPublisher configureAndGetEventPublisher() {
        EventPublisher eventPublisher = new EventPublisher();

        EventObserver consoleEventObserver = new ConsoleLoggingEventObserver();
        EventObserver jsonEventObserver = new JSONFileEventObserver("data/jsonevents.json");
        EventObserver statisticsEventObserver = configureAndGetStatisticsObserver();

        eventPublisher.registerEventObserver(consoleEventObserver);
        eventPublisher.registerEventObserver(jsonEventObserver);
        eventPublisher.registerEventObserver(statisticsEventObserver);

        return eventPublisher;
    }

    private static StatisticsGenerationEventObserver configureAndGetStatisticsObserver() {
        StatisticsGenerationEventObserver statisticsGenerationEventObserver = new StatisticsGenerationEventObserver();
        Timer timer = new Timer(true);

        StatisticsEventVisitor eventCountVisitor = new EventCountStatisticsVisitor();
        statisticsGenerationEventObserver.addStatisticsGenerator(eventCountVisitor);
        timer.schedule(new StatisticsReportTimerTask(eventCountVisitor, "data/eventcount.txt"), 0, 10 * 1000);

        return statisticsGenerationEventObserver;
    }

}
