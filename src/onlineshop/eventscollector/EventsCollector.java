package onlineshop.eventscollector;

import java.util.Timer;
import onlineshop.eventscollector.eventfactory.BackendLogsEventFactory;
import onlineshop.eventscollector.eventfactory.EventFactory;
import onlineshop.eventscollector.eventnotification.EventPublisher;
import onlineshop.eventscollector.eventnotification.JSONFileEventObserver;
import onlineshop.eventscollector.eventnotification.StatisticsGenerationEventObserver;
import onlineshop.eventscollector.events.Event;
import onlineshop.eventscollector.eventvisitor.statistics.EventCountStatisticsVisitor;
import onlineshop.eventscollector.eventvisitor.statistics.StatisticsEventVisitor;
import onlineshop.eventscollector.eventvisitor.statistics.StatisticsReportTimerTask;
import onlineshop.eventscollector.exception.InvalidEventException;

/**
 *
 * @author Hiten Vasnani
 */
public class EventsCollector {

    private static final EventPublisher eventPublisher = EventCollectorConfiguration.configureAndGetEventPublisher();

    public static void collectEvent(String eventLogString) throws InvalidEventException {
        EventFactory<String> eventCreationFactory = BackendLogsEventFactory.getInstance();
        Event event = eventCreationFactory.createEvent(eventLogString);
        if (event == null) {
            throw new InvalidEventException("Invalid event");
        }
        eventPublisher.publishEvent(event);
    }

}
