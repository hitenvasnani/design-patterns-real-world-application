package onlineshop.eventscollector.events;

import onlineshop.eventscollector.eventvisitor.EventVisitor;

/**
 *
 * @author Hiten Vasnani
 */
public abstract class Event {

    private final long occurenceTime;
    private final String type;

    public Event(long occurenceTime, String type) {
        this.occurenceTime = occurenceTime;
        this.type = type;
    }

    public long getOccurenceTime() {
        return occurenceTime;
    }

    public String getType() {
        return type;
    }
    
    public abstract <T> T accept(EventVisitor<T> eventVisitor);

}
