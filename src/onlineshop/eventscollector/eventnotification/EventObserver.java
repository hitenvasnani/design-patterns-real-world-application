package onlineshop.eventscollector.eventnotification;

import onlineshop.eventscollector.events.Event;

/**
 *
 * @author Hiten Vasnani
 */ 
public interface EventObserver {

    public void notifyEventOccured(Event event);

}
