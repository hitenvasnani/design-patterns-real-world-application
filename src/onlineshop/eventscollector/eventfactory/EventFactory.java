package onlineshop.eventscollector.eventfactory;

import onlineshop.eventscollector.events.Event;

/**
 *
 * @author Hiten Vasnani
 */
public interface EventFactory<InputType> {

    public Event createEvent(InputType input);

}
