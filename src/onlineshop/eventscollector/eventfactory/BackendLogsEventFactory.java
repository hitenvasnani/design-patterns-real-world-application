package onlineshop.eventscollector.eventfactory;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import onlineshop.eventscollector.events.AddedToCartEvent;
import onlineshop.eventscollector.events.Event;
import onlineshop.eventscollector.events.OutOfStockEvent;
import onlineshop.eventscollector.events.PurchasedEvent;
import onlineshop.eventscollector.events.ReturnedEvent;
import onlineshop.eventscollector.events.UserDeactivatedEvent;
import onlineshop.eventscollector.events.UserRegisteredEvent;

/**
 *
 * @author Hiten Vasnani
 */
public enum BackendLogsEventFactory implements EventFactory<String> {

    instance;

    public static BackendLogsEventFactory getInstance() {
        return instance;
    }

    private final static String EVENT_TYPE_SEPARATOR = ":";
    private final static String EVENT_ATTRIBUTES_DELIMITER = ", ";

    @Override
    public Event createEvent(String input) {
        try {
            String eventType = getLeftSide(input, EVENT_TYPE_SEPARATOR);
            String eventAttributes = getRightSide(input, EVENT_TYPE_SEPARATOR);
            List<String> eventAttributesList = getTokensAsList(eventAttributes, EVENT_ATTRIBUTES_DELIMITER);
            return makeEventObject(System.currentTimeMillis(), eventType, eventAttributesList);
        } catch (Exception e) {
            return null;
        }
    }

    private String getLeftSide(String string, String separator) {
        int separatorIndex = string.indexOf(separator);
        return string.substring(0, separatorIndex);
    }

    private String getRightSide(String string, String separator) {
        int separatorIndex = string.indexOf(separator);
        return string.substring(separatorIndex + 1, string.length());
    }

    private List<String> getTokensAsList(String string, String delimiter) {
        List<String> tokensList = new ArrayList<String>();
        StringTokenizer tokenizer = new StringTokenizer(string, delimiter);
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            tokensList.add(token);
        }
        return tokensList;
    }

    private Event makeEventObject(long occurenceTime, String eventType, List<String> eventAttributesList) {
        switch (eventType) {
            case UserRegisteredEvent.EVENT_TYPE_TAG:
                String country = eventAttributesList.get(0);
                return new UserRegisteredEvent(occurenceTime, country);
            case UserDeactivatedEvent.EVENT_TYPE_TAG:
                country = eventAttributesList.get(0);
                return new UserDeactivatedEvent(occurenceTime, country);
            case AddedToCartEvent.EVENT_TYPE_TAG:
                long productId = Long.parseLong(eventAttributesList.get(0));
                int quantity = Integer.parseInt(eventAttributesList.get(1));
                return new AddedToCartEvent(occurenceTime, productId, quantity);
            case OutOfStockEvent.EVENT_TYPE_TAG:
                productId = Long.parseLong(eventAttributesList.get(0));
                quantity = Integer.parseInt(eventAttributesList.get(1));
                return new OutOfStockEvent(occurenceTime, productId, quantity);
            case PurchasedEvent.EVENT_TYPE_TAG:
                productId = Long.parseLong(eventAttributesList.get(0));
                quantity = Integer.parseInt(eventAttributesList.get(1));
                country = eventAttributesList.get(2);
                return new PurchasedEvent(occurenceTime, productId, quantity, country);
            case ReturnedEvent.EVENT_TYPE_TAG:
                productId = Long.parseLong(eventAttributesList.get(0));
                quantity = Integer.parseInt(eventAttributesList.get(1));
                country = eventAttributesList.get(2);
                return new ReturnedEvent(occurenceTime, productId, quantity, country);
            default:
                return null;
        }
    }

}
