package onlineshop.eventscollector.eventvisitor;

import onlineshop.eventscollector.events.AddedToCartEvent;
import onlineshop.eventscollector.events.OutOfStockEvent;
import onlineshop.eventscollector.events.PurchasedEvent;
import onlineshop.eventscollector.events.ReturnedEvent;
import onlineshop.eventscollector.events.UserDeactivatedEvent;
import onlineshop.eventscollector.events.UserRegisteredEvent;

/**
 *
 * @author Hiten Vasnani
 */
public interface EventVisitor<T> {

    public T visit(UserRegisteredEvent userRegisteredEvent);

    public T visit(UserDeactivatedEvent userDeactivatedEvent);

    public T visit(OutOfStockEvent outOfStockEvent);

    public T visit(AddedToCartEvent addedToCartEvent);

    public T visit(PurchasedEvent purchasedEvent);

    public T visit(ReturnedEvent returnedEvent);

}
