package onlineshop.eventscollector.eventvisitor.statistics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TimerTask;

/**
 *
 * @author Hiten Vasnani
 */
public class StatisticsReportTimerTask extends TimerTask {

    private final StatisticsEventVisitor statisticsEventVisitor;
    private final String filename;

    public StatisticsReportTimerTask(StatisticsEventVisitor statisticsEventVisitor, String filename) {
        this.statisticsEventVisitor = statisticsEventVisitor;
        this.filename = filename;
    }

    @Override
    public void run() {
        File file = new File(filename);
        file.getParentFile().mkdirs();
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new FileWriter(file));
            output.write(statisticsEventVisitor.getReport());
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
