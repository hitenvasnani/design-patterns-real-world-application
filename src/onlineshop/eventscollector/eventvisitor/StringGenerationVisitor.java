package onlineshop.eventscollector.eventvisitor;

import java.util.LinkedHashMap;
import java.util.Map;
import onlineshop.eventscollector.events.AddedToCartEvent;
import onlineshop.eventscollector.events.OutOfStockEvent;
import onlineshop.eventscollector.events.PurchasedEvent;
import onlineshop.eventscollector.events.ReturnedEvent;
import onlineshop.eventscollector.events.UserDeactivatedEvent;
import onlineshop.eventscollector.events.UserRegisteredEvent;
import onlineshop.eventscollector.eventstringstrategy.StringGenerationStrategy;

/**
 *
 * @author Hiten Vasnani
 */
public class StringGenerationVisitor implements EventVisitor<String> {

    private final StringGenerationStrategy stringGenerationStrategy;

    public StringGenerationVisitor(StringGenerationStrategy stringGenerationStrategy) {
        this.stringGenerationStrategy = stringGenerationStrategy;
    }

    @Override
    public String visit(UserRegisteredEvent userRegisteredEvent) {
        Map<String, String> attributesMap = new LinkedHashMap<String, String>();
        attributesMap.put("country", userRegisteredEvent.getCountry());
        return stringGenerationStrategy.getString(userRegisteredEvent, attributesMap);
    }

    @Override
    public String visit(UserDeactivatedEvent userDeactivatedEvent) {
        Map<String, String> attributesMap = new LinkedHashMap<String, String>();
        attributesMap.put("country", userDeactivatedEvent.getCountry());
        return stringGenerationStrategy.getString(userDeactivatedEvent, attributesMap);
    }

    @Override
    public String visit(OutOfStockEvent outOfStockEvent) {
        Map<String, String> attributesMap = new LinkedHashMap<String, String>();
        attributesMap.put("product_id", outOfStockEvent.getProductId() + "");
        attributesMap.put("quantity", outOfStockEvent.getQuantity() + "");
        return stringGenerationStrategy.getString(outOfStockEvent, attributesMap);
    }

    @Override
    public String visit(AddedToCartEvent addedToCartEvent) {
        Map<String, String> attributesMap = new LinkedHashMap<String, String>();
        attributesMap.put("product_id", addedToCartEvent.getProductId() + "");
        attributesMap.put("quantity", addedToCartEvent.getQuantity() + "");
        return stringGenerationStrategy.getString(addedToCartEvent, attributesMap);
    }

    @Override
    public String visit(PurchasedEvent purchasedEvent) {
        Map<String, String> attributesMap = new LinkedHashMap<String, String>();
        attributesMap.put("product_id", purchasedEvent.getProductId() + "");
        attributesMap.put("quantity", purchasedEvent.getQuantity() + "");
        attributesMap.put("country", purchasedEvent.getCountry() + "");
        return stringGenerationStrategy.getString(purchasedEvent, attributesMap);
    }

    @Override
    public String visit(ReturnedEvent returnedEvent) {
        Map<String, String> attributesMap = new LinkedHashMap<String, String>();
        attributesMap.put("product_id", returnedEvent.getProductId() + "");
        attributesMap.put("quantity", returnedEvent.getQuantity() + "");
        attributesMap.put("country", returnedEvent.getCountry() + "");
        return stringGenerationStrategy.getString(returnedEvent, attributesMap);
    }
}
